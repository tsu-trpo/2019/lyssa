package space.deflate.lyssa.storage;

import android.util.SparseArray;

import java.util.HashMap;
import java.util.Set;

import space.deflate.lyssa.model.State;
import space.deflate.lyssa.model.Storage;

public class DummyStorage implements Storage {
    private SparseArray<DummyBackedState> states = new SparseArray<>();

    public DummyStorage() {
        states.append(0, new DummyBackedState("initial", new HashMap<String, Integer>() {{
            put("first", 1);
            put("second", 2);
        }}));
        states.append(1, new DummyBackedState("first", new HashMap<String, Integer>() {{
            put("second", 2);
            put("final", 3);
        }}));
        states.append(2, new DummyBackedState("second", new HashMap<String, Integer>() {{
            put("initial", 0);
            put("final", 3);
        }}));
        states.append(3, new DummyBackedState("final", new HashMap<String, Integer>()));
    }

    @Override
    public State getInitialState() {
        return states.get(0);
    }

    private class DummyBackedState implements State {
        private HashMap<String, Integer> nextStatesIds;
        private String text;

        DummyBackedState(String text, HashMap<String, Integer> nextStatesIds) {
            this.text = text;
            this.nextStatesIds = nextStatesIds;
        }

        @Override
        public String getText() {
            return text;
        }

        @Override
        public Set<String> getActions() {
            return nextStatesIds.keySet();
        }

        @Override
        public State nextState(String action) {
            Integer id = nextStatesIds.get(action);
            if (id == null) {
                throw new RuntimeException(String.format("Cannot find id for action '%s'", action));
            }
            return states.get(id);
        }
    }
}
