package space.deflate.lyssa.model;

import java.util.Set;

public interface State {
    String getText();

    Set<String> getActions();

    State nextState(String action);
}
