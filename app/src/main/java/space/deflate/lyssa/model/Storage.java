package space.deflate.lyssa.model;

public interface Storage {
    State getInitialState();
}
