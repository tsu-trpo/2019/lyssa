package space.deflate.lyssa.presentation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import space.deflate.lyssa.R;
import space.deflate.lyssa.logic.QuestLogic;
import space.deflate.lyssa.model.QuestHandler;
import space.deflate.lyssa.model.State;

public class MainActivity extends AppCompatActivity implements QuestHandler {

    private QuestLogic questLogic;

    private TextView textbox;

    private ArrayList<Button> btnPool = new ArrayList<>();

    public void setState(State state) {
        if (state == null) {
            throw new RuntimeException("state == null");
        }

        textbox.setText(state.getText());
        String[] actions = state.getActions().toArray(new String[0]);

        // fill actions
        for (int i = 0; i < actions.length; i++) {
            Button btn = btnPool.get(i);
            if (btn != null) {
                btn.setVisibility(View.VISIBLE);
                btn.setText(actions[i]);
            }
        }

        // hide unused buttons
        for (int i = actions.length; i < btnPool.size(); i++) {
            Button btn = btnPool.get(i);
            if (btn != null) {
                btn.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textbox = findViewById(R.id.message);

        // Initialize button pool
        btnPool.add((Button) findViewById(R.id.action1));
        btnPool.add((Button) findViewById(R.id.action2));
        btnPool.add((Button) findViewById(R.id.action3));

        // Initialize logic instance
        questLogic = new QuestLogic(this);

        // Add event listeners
        for (int i = 0; i < btnPool.size(); i++) {
            final Button btn = btnPool.get(i);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    questLogic.actionPerformed(btn.getText().toString());
                }
            });
        }

        // Start game
        questLogic.resetState();
    }

}
