package space.deflate.lyssa.logic;

import space.deflate.lyssa.model.QuestHandler;
import space.deflate.lyssa.model.State;
import space.deflate.lyssa.storage.DummyStorage;
import space.deflate.lyssa.model.Storage;

public class QuestLogic {
    private State currentState = null;
    private State initialState;
    private QuestHandler handler;
    private Storage storage;

    public QuestLogic(QuestHandler handler) {
        this.handler = handler;
        this.storage = new DummyStorage(); // TODO: implement DI
        initialState = this.storage.getInitialState();
    }

    private void setState(State state) {
        currentState = state;
        this.handler.setState(currentState);
    }

    public void resetState() {
        setState(initialState);
    }

    public void actionPerformed(String action) {
        setState(currentState.nextState(action));
    }
}
